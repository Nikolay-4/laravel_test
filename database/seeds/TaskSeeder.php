<?php

use Illuminate\Database\Seeder;
use App\Task;
class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('INSERT INTO  tasks (description, status, user_id, tasklist_id) VALUES (?, ?, ?, ?)', [
            'task1',
            'open',
            '1',
            '1'
        ]);

        //2
        DB::table('tasks')->insert([
            [
                'description' => 'task2',
                'status' => 'open',
                'user_id' => '1',
                'tasklist_id' => '1'
            ],
            [
                'description' => 'task3',
                'status' => 'open',
                'user_id' => '1',
                'tasklist_id' => '2'
            ],
        ]);

        //3
        Task::create(
            [
                'description' => 'task4',
                'status' => 'closed',
                'user_id' => '1',
                'tasklist_id' => '2'
            ]
        );
    }
}
