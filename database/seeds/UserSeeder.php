<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;
use App\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u1 = User::create(
            [
                'name' => 'user1',
                'email' => 'email1',
                'password' => bcrypt('123')
            ]
        );

        $u2 = User::create(
            [
                'name' => 'user2',
                'email' => 'email2',
                'password' => bcrypt('123')
            ]
        );

        $a1 = User::create(
            [
                'name' => 'admin1',
                'email' => 'email3',
                'password' => bcrypt('123')
            ]
        );

        $s1 = User::create(
            [
                'name' => 'superadmin1',
                'email' => 'email4',
                'password' => bcrypt('123')
            ]
        );

        $u1->createToken('MyApp');
        $u2->createToken('MyApp');
        $a1->createToken('MyApp');
        $s1->createToken('MyApp');

        $user = new Role();
        $user->name = "user";
        $user->display_name = "user";
        $user->save();

        $admin = new Role();
        $admin->name = "admin";
        $admin->display_name = "admin";
        $admin->save();

        $superAdmin = new Role();
        $superAdmin->name = "super_admin";
        $superAdmin->display_name = "super admin";
        $superAdmin->save();

        $u1->attachRole($user);
        $u2->attachRole($user);

        $a1->attachRole($user);
        $a1->attachRole($admin);

        $s1->attachRole($user);
        $s1->attachRole($admin);
        $s1->attachRole($superAdmin);


        $crudTask = new Permission();
        $crudTask->name = 'crud_task';
        $crudTask->display_name = 'Create, update, delete task';
        $crudTask->save();

        $crudTaskList = new Permission();
        $crudTaskList->name = 'crud_tasklist';
        $crudTaskList->display_name = 'Create, update, delete tasklist';
        $crudTaskList->save();

        $adminPanel = new Permission();
        $adminPanel->name = 'admin_panel';
        $adminPanel->display_name = 'Access to admin panel';
        $adminPanel->save();

        $editTaskListLimit = new Permission();
        $editTaskListLimit->name = 'edit_tasklist_limit';
        $editTaskListLimit->display_name = 'Edit task list limits';
        $editTaskListLimit->save();

        $user->attachPermission($crudTask);
        $user->attachPermission($crudTaskList);

        $admin->attachPermission($adminPanel);

        $superAdmin->attachPermission($superAdmin);
    }
}
