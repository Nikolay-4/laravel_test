<?php

use Illuminate\Database\Seeder;
use App\TaskList;

class TaskListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TaskList::create(
            [
                'name' => 'tasklist1',
                'user_id' => '1',
            ]
        );

        TaskList::create(
            [
                'name' => 'tasklist2',
                'user_id' => '1',
            ]
        );
    }
}
