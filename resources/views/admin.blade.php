@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>{{$tablename}}</h1>
@stop

@section('content')

    <div class="box">
        <table id="taskListTable" class="table table-striped datatables">
            <thead>
            <tr>
                @foreach ($columns as $column)
                    <td>
                        {!! $column !!}
                    </td>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach ($collection as $row)
                <tr>
                    @foreach ($row as $cell)
                        <td>
                            {!! $cell !!}
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/purple.css" rel="stylesheet">
@stop

@section('js')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#taskListTable').dataTable();
        });
    </script>
    {{--<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>--}}
    {{--<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>--}}
@stop