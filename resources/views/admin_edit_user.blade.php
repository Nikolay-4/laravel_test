@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edit limits</h1>
@stop

@section('content')

    <div class="box row">

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li> {{$error}}</li>
                        @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" class="form-group" action="{{action('AdminController@editUserLimit')}}">
            {{ csrf_field() }}
            <div class="form-group">
                <p>{{$user_name}} already has {{$task_list_cnt}} task lists </p>
                <label for="limit_form"> New limit</label>
                <input id="limit_form " class="form-control" type="number" name="limit" value="{{$current_limit}}">
                <input name="user_id" hidden value="{{$user_id}}">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/purple.css" rel="stylesheet">
@stop

@section('js')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#taskListTable').dataTable();
        });
    </script>
    {{--<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>--}}
    {{--<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>--}}
@stop