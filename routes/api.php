<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Task;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//
//Route::get('task', ['uses' => 'TaskResourceController@index', 'as' => 'get_task']);
//Route::get('task/{id}', 'TaskResourceController@show');
//Route::post('task', 'TaskResourceController@store');
//Route::put('task/{id}', 'TaskResourceController@update');
//Route::delete('task/{id}', 'TaskResourceController@delete');

Route::resource('/task', 'TaskResourceController', ['except' => ['create', 'edit']]);

//
//Route::get('tasklist', 'TaskListResourceController@index');
//Route::get('tasklist/{id}', 'TaskListResourceController@show');
//Route::post('tasklist', 'TaskListResourceController@store');
//Route::put('tasklist/{id}', 'TaskListResourceController@update');
//Route::delete('tasklist/{id}', 'TaskListResourceController@delete');

Route::resource('/tasklist', 'TaskListResourceController', ['except' => ['create', 'edit']]);


//
//Route::group(['prefix' => '1task', 'middleware' => 'mymiddleware'], function (){
//    Route::get('', ['as' => 'tasks_route', function (){
//        $content = view("home")->withData('testData')->render();
//        return (new Response($content, 201))->header('ContentType', "json");
//        return response($content, 200)-header('MyHeader', "2223");
//        return response()->json(['test' => 'tes2']);
//        return response()->view('home', ['data' => 'd44']);

//        return new \Illuminate\Http\RedirectResponse('/'); // redirect
//        return redirect('/'); // with helper
//        return back(); // back redirect
//        return redirect()->route('task_route');
//        return redirect()->action('TaskListResourceController@show');
//        return response()->badReq("error");
//    }]);
//
//    Route::get('/{id}', ['as' => 'task_route', function ($id){
//        echo "<pre>";
//        echo "task ".$id."\n";
//        echo "<a href=\"".(route('tasks_url'))."\"> all tasks </a>";
//        echo "</pre>";
//    }]);
//});
//
//
//Route::group(['prefix' => 'admin'], function (){
//   Route::get('users', function (){
//       echo "list users";
//   });
//
//   Route::get('user/{id}', function ($id){
//       echo "user" + $id;
//   });
//});

//
//Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');
//Route::group(['middleware' => 'auth:api'], function(){
//    Route::post('get-details', 'PassportController@getDetails');
//});