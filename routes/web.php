<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::group(['prefix' => 'admin'], function (){
    Route::match(['post', 'get'], '/login', ['uses' => 'AdminController@login', 'as' => 'login_route']);
    Route::get('/', 'AdminController@showTaskLists');
    Route::post('/logout', ['uses' => 'AdminController@logout', 'as' => 'logout_route']);
    Route::get('/tasklists', 'AdminController@showTaskLists');
    Route::get('/tasks', 'AdminController@showTasks');
    Route::get('/users', 'AdminController@showUsers');
    Route::get('/edit_limit', 'AdminController@editUserLimitForm');
    Route::post('/edit_limit', 'AdminController@editUserLimit');

});
