<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public static $TASK_STATUS = ["open", "closed"];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function task_list(){
        return $this->belongsTo('App\TaskList');
    }

}
