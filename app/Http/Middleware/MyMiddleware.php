<?php

namespace App\Http\Middleware;

use Closure;

class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        print_r($request->userAgent());
        echo "<hr>";
        $response =  $next($request);

        echo "<hr> end of mymiddleware";
        return $response;
    }
}
