<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use App\TaskList;
use App\Task;
use App\User;
use Entrust;
use Auth;
use Validator;

class AdminController extends Controller
{

    public function __construct()
    {
        Auth::shouldUse('web');
    }

    public function showTaskLists(Request $request){
        if(!Entrust::hasRole('admin')){
            return redirect()->route('login_route');
        }
        $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : null;
        if($user_id != null) {
            try {
                $content = TaskList::where('user_id', $user_id)->get();
            } catch (Exception $e) {
                $content = [];
            }
        } else{
            $content = TaskList::all();
        }
        $collection = [];
        $columns = [];
        if($content->count() > 0){
            $collection= $content->toArray();
            $columns = array_keys($collection[0]);
            $columns[] = '';
        }
        $table_name = "Task lists";
        return view('admin_tasklists')->withCollection($collection)->withColumns($columns)->withTablename($table_name);
    }

    public function showTasks(Request $request){
        if(!Entrust::hasRole('admin')){
            return redirect()->route('login_route');
        }
        $tasklist_id= isset($_GET['tasklist_id']) ? $_GET['tasklist_id'] : null;
        if($tasklist_id!= null) {
            try {
                $content = Task::where('tasklist_id', $tasklist_id)->get();
            } catch (Exception $e) {
                $content = [];
            }
        } else{
            $content = Task::all();
        }

        $collection = [];
        $columns = [];
        if($content->count() > 0){
            $collection= $content->toArray();
            $columns = array_keys($collection[0]);
        }
        $table_name = "Tasks";
        return view('admin')->withCollection($collection)->withColumns($columns)->withTablename($table_name);
    }

    public function showUsers(Request $request){
        if(!Entrust::hasRole('admin')){
            return redirect()->route('login_route');
        }
        $content = User::get(['id', 'name', 'email', 'tasklist_limit']);
        $collection = [];
        $columns = [];
        if($content->count() > 0){
            $collection= $content->toArray();
            $columns = array_keys($collection[0]);
            $columns[] = '';
        }
        $table_name = "Users";
        return view('admin_users')->withCollection($collection)->withColumns($columns)->withTablename($table_name);
    }

    public function editUserLimitForm(Request $request){
        if(!Entrust::hasRole('super_admin')){
            return "<h1>Permission denied</h1>";
        }
        try{
            $user_id = $request->user_id;
            $current_limit = $request->limit;
            $user = User::where('id', $user_id)->first();
            $task_list_cnt = $user->taskLists->count();
        } catch(\Exception $e){
            return response("User id required", 400);
        }

        return view('admin_edit_user', [
            'user_id' => $user_id,
            'user_name' => $user->name,
            'current_limit' => $current_limit,
            'task_list_cnt' => $task_list_cnt
        ]);
    }

    public function editUserLimit(Request $request){
        if(!Entrust::hasRole('super_admin')){
            return "<h1>Permission denied</h1>";
        }
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'limit' => 'required|integer|min:0'
        ]);

        $fail_flag = false;

        try{
            $user = User::where('id', $request->user_id)->first();
            $task_list_cnt = $user->taskLists->count();
        } catch(\Exception $e){
            return response("User id required", 400);
        }

        $new_limit = $request->limit;
        if($new_limit < $task_list_cnt){
            $fail_flag = true;
        }

        $validator->after(function ($validator) use($fail_flag, $task_list_cnt){
            if($fail_flag)
                $validator->errors()->add('limit', 'new limit must be more or equal '.$task_list_cnt);
        });

        if($validator->fails() or $fail_flag){
            return redirect()->back()->withErrors($validator);
        }
        $user->tasklist_limit = $new_limit;
        $user->save();

        return redirect()->action('AdminController@showUsers');
    }


    public function login(Request $request){
        if($request->method() == 'POST'){
            if($this->attemptLogin($request)){
                return redirect('admin/');
            }
            return redirect()->route('login_route');
        }
        return view("auth.login");
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect()->route('login_route');
    }

    protected function attemptLogin($request){
        return Auth::guard()->attempt(
            $request->only('email', 'password'),
            $request->filled('remember')
        );
    }
}
