<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TaskList;
use App\User;
use Illuminate\Http\Response;
use Auth;
use Validator;

class TaskListResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TaskList::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tasklist = TaskList::find($id);
        if ($tasklist == null)
            return response()->json('invalid id', 400);
        return $tasklist;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if(!Auth::check() or !$user->can('crud_task')){
            return response()->json(['errors' => 'permission denied'], 400);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(), 400);
        }

        $tasklists_count = $user->taskLists->count();
        $tasklists_limit = $user->tasklist_limit;
        if($tasklists_count >= $tasklists_limit){
            return response()->json(['errors' => 'task lists limit for user = '.$tasklists_limit], 400);
        }

        $tasklist = new TaskList;
        $tasklist->name = $request->name;
        $user->taskLists()->save($tasklist);
        return $tasklist;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if(!Auth::check() or !$user->can('crud_task')){
            return response()->json(['errors' => 'permission denied'], 400);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'user_id' => 'required|exists:users,id'
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(), 400);
        }

        $upd_user = User::where('id', $request->user_id)->first();
        $tasklists_count = $upd_user->taskLists->count();
        $tasklists_limit = $upd_user->tasklist_limit;
        if($tasklists_count >= $tasklists_limit){
            return response()->json(['errors' => 'task lists limit for user = '.$tasklists_limit], 400);
        }

        $tasklist = TaskList::findOrFail($id);
        $tasklist->name = $request->name;
        $tasklist->user_id = $request->user_id;
        $tasklist->save();
        return $tasklist;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = Auth::user();
        if(!Auth::check() or !$user->can('crud_task')){
            return response()->json(['errors' => 'permission denied'], 400);
        }

        $tasklist = TaskList::find($id);
        if ($tasklist == null)
            return response()->json('invalid id', 400);
        $tasklist->delete();

        return response('', 204);
    }
}
