<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Illuminate\Validation\Rule;
use Validator;
use DB;
use Auth;

class TaskResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
//        $tasks = DB::table('tasks')->get(); // get all rows
//        $t = DB::table('tasks')->first();
//        $t = DB::table('tasks')->value('name');
//        $t = DB::table('tasks')->select('name', 'descr')->get();


        $tasks = Task::all();
        return $tasks;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        if ($task == null)
            //abort(400, "invalid id");
            return response()->json('invalid id', 400);
        return $task;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if(!Auth::check() or !$user->can('crud_task')){
            return response()->json(['errors' => 'permission denied'], 400);
        }

        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'status' => ['required',
                Rule::in(Task::$TASK_STATUS)],
            'tasklist_id' => 'exists:task_lists,id'
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(), 400);
        }

        $task = new Task;
        $task->description = $request->description;
        $task->status = $request->status;
        $task->tasklist_id = $request->tasklist_id;
        $user->tasks()->save($task);
        return $task;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if(!Auth::check() or !$user->can('crud_task')){
            return response()->json(['errors' => 'permission denied'], 400);
        }

        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'user_id' => 'required|exists:users,id',
            'status' => ['required',
                Rule::in(Task::$TASK_STATUS)],
            'tasklist_id' => 'required|exists:task_lists,id'
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(), 400);
        }

        $task = Task::findOrFail($id);
        $task->description = $request->description;
        $task->status = $request->status;
        $task->user_id = $request->user_id;
        $task->tasklist_id = $request->tasklist_id;
        $task->save();
        return $task;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = Auth::user();
        if(!Auth::check() or !$user->can('crud_task')){
            return response()->json(['errors' => 'permission denied'], 400);
        }

        $task = Task::find($id);
        if ($task == null)
            return response()->json('invalid id', 400);
        $task->delete();

        return response('task '.$id.' deleted', 204);
    }
}