<?php

namespace App\Providers;

use Response;
use DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('myRes', function ($data){
            return Response::make($data);
        });

        Response::macro('badReq', function ($data){
            return response($data, 400);
        });


        // show sql for debug sql
//        DB::listen(function ($query){
//            dump($query->sql);
//            dump($query->bindings);
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
