<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

interface ITest{
    public function toStr($data);
}

class TestClass implements ITest {
    public function toStr($data)
    {
        return "test ".$data;
    }
}

class TestProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $obj = new TestClass();
        $this->app->instance('App\Providers\ITest', $obj);

        // получить реализацию объекта
//        $this->app['App\Providers\ITest'];
        // или
//        $this->app->make('App\Providers\ITest');


        //привязка интерфейса к реализации
//        $this->app->bind('App\Providers\ITest', function (){
//            return new TestClass();
//        });

//         или

        $this->app->bind('App\Providers\ITest', 'App\Providers\TestClass');

    }
}
